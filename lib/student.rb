class Student
  def initialize(first_name,last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def courses
    @courses
  end

  def name
    @first_name + ' ' + @last_name
  end

  def enroll(new_course)
    return if @courses.include?(new_course)
    raise "ERROR: COURSE CONFLICT" if has_conflict?(new_course)
    @courses << new_course
    new_course.students << self
  end

  def course_load
    load_hash = Hash.new(0)
    @courses.each do |course|
      load_hash[course.department] += course.credits
    end

    load_hash

  end

  def has_conflict?(new_course)
    @courses.each do |course|
      if course.conflicts_with?(new_course)
        return true
      end
    end

    false
  end


end
